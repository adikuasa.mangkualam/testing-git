# Practice Spring Boot REST

## Tools
- Intelij IDEA
- Data Grip
- MySQL
- JDK 11

### Plugin Intelij IDEA
- Lombok Annotation

## Step to Setting Up
1. Create user for MySQL database
2. Create database
3. Pull the latest source code from repository

## How to contribute?
1. Writing Service
2. Writing Repository
3. Writing Model
4. Writing Controller
5. Writing Configuration
6. Writing Test(s)
7. Writing Exception
8. Writing Validator