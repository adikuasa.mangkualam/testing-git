package com.example.practice.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class Application {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Map<String, String> main() {
        Map<String, String> map = new HashMap<>();
        map.put("message", "ok");
        return map;
    }

}
