package com.example.practice.controller;

import com.example.practice.model.HumanModel;
import com.example.practice.service.HumanService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/human")
public class HumanController {

    private final HumanService humanService;

    public HumanController(HumanService humanService) {
        this.humanService = humanService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<HumanModel> getAll() {
        return humanService.getAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public HumanModel getById(String id) {
        // TODO: Call service to get data from DB by ID
        return new HumanModel();
    }

}
