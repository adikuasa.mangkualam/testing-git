package com.example.practice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@NoArgsConstructor
@MappedSuperclass
public abstract class BaseModel {

    @JsonProperty("created_at")
    @Column(name = "CREATED_AT", nullable = false)
    @Setter
    @Getter
    private Timestamp createdAt;

    @JsonProperty("updated_at")
    @Column(name = "UPDATED_AT")
    @Setter
    @Getter
    private Timestamp updatedAt;

    @JsonIgnore
    @Column(name = "DELETED_AT")
    @Setter
    @Getter
    private Timestamp deletedAt;

    @SuppressWarnings("unused")
    @PrePersist
    void onCreate() {
        createdAt = Timestamp.valueOf(LocalDateTime.now());
    }

    @SuppressWarnings("unused")
    @PreUpdate
    void onUpdate() {
        updatedAt = Timestamp.valueOf(LocalDateTime.now());
    }

}
