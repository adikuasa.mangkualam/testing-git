package com.example.practice.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "M_HUMAN")
public class HumanModel extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Column(name = "NAME", nullable = false)
    @Getter
    @Setter
    private String name;

    @Column(name = "AGE", nullable = false)
    @Getter
    @Setter
    private String age;

    @Column(name = "GENDER", nullable = false)
    @Getter
    @Setter
    private String gender;

}
