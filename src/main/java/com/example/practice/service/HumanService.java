package com.example.practice.service;

import com.example.practice.model.HumanModel;
import com.example.practice.repository.HumanRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HumanService {

    private final HumanRepository humanRepository;

    public HumanService(HumanRepository humanRepository) {
        this.humanRepository = humanRepository;
    }

    public List<HumanModel> getAll() {
        return humanRepository.findAll();
    }

}
