package com.example.practice.repository;

import com.example.practice.model.HumanModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HumanRepository extends JpaRepository<HumanModel, Long> {

    @Query(value = "SELECT * FROM M_HUMAN", nativeQuery = true)
    List<HumanModel> findAll();

}
