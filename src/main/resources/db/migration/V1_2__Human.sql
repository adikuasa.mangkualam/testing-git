SET autocommit = OFF;
START TRANSACTION;
    CREATE TABLE M_HUMAN
    (
        id                  BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        created_at          TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
        updated_at          TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NULL,
        deleted_at          TIMESTAMP NULL,
        name                VARCHAR(255) NOT NULL,
        age                 VARCHAR(3) NOT NULL,
        gender              VARCHAR(1) NOT NULL
    );
COMMIT;